# *Polls.go - A place where anyone can share their opinion*

## _View the questions you are interested in -_ 
![ss1](readme_res/ss1.png)

## _Vote for your favourite ones -_
![ss2](readme_res/ss2.png)

## _Get current results instantly -_
![ss3](readme_res/ss3.png)